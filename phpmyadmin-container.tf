resource "docker_image" "phpmyadmin_container_image" {
  name          = data.docker_registry_image.phpmyadmin.name
  pull_triggers = [data.docker_registry_image.phpmyadmin.sha256_digest]
  keep_locally  = true
}

resource "docker_container" "phpmyadmin_container" {
  image = docker_image.phpmyadmin_container_image.latest
  name  = var.container_phpmyadmin_name
    ports {
    internal = 80
    external = 82
  }

  env = [
    "PMA_ARBITRARY=1",
    "PMA_ABSOLUTE_URI=${var.PMA_ABSOLUTE_URI}"
  ]


  networks_advanced {
    name = var.network_name
  }



}
