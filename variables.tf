variable "docker_phpmyadmin_image" {
  default = "phpmyadmin/phpmyadmin:4.7.7-1"
}
variable "container_phpmyadmin_name" {
  description = ""
}
variable "network_name" {
  description = "nom du conteneur"
}
variable "PMA_ABSOLUTE_URI" {
  description = ""
}
