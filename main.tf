terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
  }
}
data "docker_registry_image" "phpmyadmin" {
  name = var.docker_phpmyadmin_image
}